import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProdutosComponent } from './components/produtos/produtos.component';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FilterPipe } from './components/produtos/filter.pipe';
import { FormProdutosComponent } from './components/form-produtos/form-produtos.component';
import {AppRoutingModule} from './app-routing';
import { DomingasComponent } from './components/domingas/domingas.component';
import { RegistarDomingaComponent } from './components/registar-dominga/registar-dominga.component';
import { RegistarIrmaoComponent } from './components/registar-irmao/registar-irmao.component';
import { NomeIrmaoPipe } from './components/registar-dominga/nome-irmao.pipe';
import { DimingapipePipe } from './components/domingas/dimingapipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ProdutosComponent,
    FilterPipe,
    FormProdutosComponent,
    DomingasComponent,
    RegistarDomingaComponent,
    RegistarIrmaoComponent,
    NomeIrmaoPipe,
    DimingapipePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
   ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
