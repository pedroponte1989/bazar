import {Irmao} from "./irmao";

export interface Dominga {
  idDominga: string;
  irmao_idIrmao: Irmao;
  ano: number;
  numero:number;
  diferenca:number;
  bit:number;
}
