import {Categoria} from "./categoria";

export interface JsonProduto {
  idprodutos:	number
  nome:string
  categoria: Categoria;
}
