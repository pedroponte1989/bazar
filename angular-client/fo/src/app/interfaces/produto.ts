export interface Produto {
  id: string;
  descricao: string;
  categoria: number;
  qtd: number;
}

