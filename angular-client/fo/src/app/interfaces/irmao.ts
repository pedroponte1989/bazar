export interface Irmao {
    idirmao: number;
    nome: string;
    rua: string;
    nporta: string;
    freguesia: string;
    visivel: string;
}
