import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProdutosComponent} from './components/produtos/produtos.component';
import {FormProdutosComponent} from './components/form-produtos/form-produtos.component';
import {DomingasComponent} from './components/domingas/domingas.component';
import {RegistarDomingaComponent} from './components/registar-dominga/registar-dominga.component';
import {RegistarIrmaoComponent} from './components/registar-irmao/registar-irmao.component';

const appRoutes: Routes = [
  {
    path: 'novoProduto',
    component: FormProdutosComponent
  },
  {
    path: 'produtos',
    component: ProdutosComponent
  },
  {
    path: 'dominga',
    component: DomingasComponent
  } ,
  {
    path: 'dominga/registar',
    component: RegistarDomingaComponent
  } ,
  {
    path: 'dominga/registarIrmao',
    component: RegistarIrmaoComponent
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: true, // <-- debugging purposes only

      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
