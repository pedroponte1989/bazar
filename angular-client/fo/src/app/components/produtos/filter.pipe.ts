import { Pipe, PipeTransform } from '@angular/core';
import {Produto} from '../../interfaces/produto';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(produtos: Produto[], numero: string): any[] {
    if(!produtos) return [];
    if(!numero) return produtos;

    return produtos.filter( function(produtos){

      return produtos.id.toString().includes(numero.toString());
    });
  }
}
