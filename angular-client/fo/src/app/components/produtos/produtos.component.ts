import { Component, OnInit } from '@angular/core';
import {Produto} from "../../interfaces/produto";
import {ProdutoService} from "./produto.service";

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css'],
  providers: [ProdutoService]
})
export class ProdutosComponent implements OnInit {
  produtos: Produto[];
  numero: string;
  constructor(private produtoservice: ProdutoService) { }

  ngOnInit() {
    this.getProdutos();
  }

  getProdutos(){
    this.produtoservice.getConfig()
      .subscribe(data => this.produtos = data);
  }
  addProduto() {
    let produto: Produto = {id: this.numero, descricao: null, categoria: null, qtd: -1};
    this.produtoservice.putMovimento(produto).subscribe(data => this.produtos = data);
  }
}
