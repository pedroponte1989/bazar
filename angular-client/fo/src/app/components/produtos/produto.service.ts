import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {Produto} from '../../interfaces/produto';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};
@Injectable()
export class ProdutoService {

  constructor(private http: HttpClient) { }

  getConfig(): Observable<Produto[]> {
    return this.http.get<Produto[]>( '/api/saida/lista');
  }
  putMovimento(produto: Produto) {
    return this.http.put<Produto[]>( 'http://localhost/api/saida/entrada/', produto, httpOptions );
  }

}
