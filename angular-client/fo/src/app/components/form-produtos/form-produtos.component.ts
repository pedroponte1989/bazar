import { Component, OnInit } from '@angular/core';
import {Produto} from "../../interfaces/produto";
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import {FormProdutosService} from "./form-produtos.service";
import {Categoria} from "../../interfaces/categoria";
@Component({
  selector: 'app-form-produtos',
  templateUrl: './form-produtos.component.html',
  styleUrls: ['./form-produtos.component.css'],
  providers:[FormProdutosService]
})
export class FormProdutosComponent implements OnInit {
  categoria: Categoria[];
  produto: Produto = { id:'', descricao: '', qtd:0, categoria:0};
  constructor(private  formProdutosService:FormProdutosService) { }

  ngOnInit() {
    this.getCategorias();
  }

  addProduto(){
    this.formProdutosService.putMovimento(this.produto).subscribe();
    this.produto = { id:'', descricao: '', qtd:0, categoria:0};

  }

  getUltimoProduto(val:number) {
    this.formProdutosService.getUltimoProduto(val).subscribe(res=> this.produto.id = ''+(res.idprodutos+1));

  }
  getCategorias(){
    this.formProdutosService.getCategorias().subscribe(res=> this.categoria=res);
  }
}
