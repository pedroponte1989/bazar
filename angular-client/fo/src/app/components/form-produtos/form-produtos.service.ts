import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {Produto} from "../../interfaces/produto";
import {JsonProduto} from "../../interfaces/json-produto";
import {Categoria} from "../../interfaces/categoria";
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class FormProdutosService {

  constructor(private http: HttpClient) { }

  getCategorias(): Observable<Categoria[]> {
    return this.http.get<Categoria[]>( 'http://localhost/api/categoria/lista');
  }
  getUltimoProduto(id:number): Observable<JsonProduto> {
    return this.http.get<JsonProduto>( 'http://localhost/api/produtos/ultimoProduto/'+ id);
  }
  putMovimento(produto:Produto) {
    return this.http.put<Produto[]>( 'http://localhost/api/produtos/Novo/',produto, httpOptions );
  }

}
