import { TestBed, inject } from '@angular/core/testing';

import { FormProdutosService } from './form-produtos.service';

describe('FormProdutosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormProdutosService]
    });
  });

  it('should be created', inject([FormProdutosService], (service: FormProdutosService) => {
    expect(service).toBeTruthy();
  }));
});
