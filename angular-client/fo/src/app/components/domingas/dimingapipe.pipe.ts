import { Pipe, PipeTransform } from '@angular/core';
import {Dominga} from '../../interfaces/dominga';

@Pipe({
  name: 'dimingapipe'
})
export class DimingapipePipe implements PipeTransform {

  transform(dominga: Dominga[], numero: number): any[] {
    if(!dominga) return [];
    if(!numero) return dominga;

    // tslint:disable-next-line:no-shadowed-variable only-arrow-functions only-arrow-functions
    return dominga.filter( function(dominga) {

      // @ts-ignore
      return dominga.bit !== numero;
    });
  }

}
