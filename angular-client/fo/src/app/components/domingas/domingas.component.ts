import { Component, OnInit } from '@angular/core';
import {DocmingaService} from "./docminga.service";
import {Dominga} from "../../interfaces/dominga";

@Component({
  selector: 'app-domingas',
  templateUrl: './domingas.component.html',
  styleUrls: ['./domingas.component.css'],
  providers: [DocmingaService]

})

export class DomingasComponent implements OnInit {
  domingas: Dominga[];
  data = new Date().getFullYear();
  constructor(private docmingaService: DocmingaService) { }
  numero: number;
  ngOnInit() {

    this.getDomingas();
  }
  getDomingas() {
    this.docmingaService.getlista().subscribe(res => this.domingas = res);
  }

}
