import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomingasComponent } from './domingas.component';

describe('DomingasComponent', () => {
  let component: DomingasComponent;
  let fixture: ComponentFixture<DomingasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomingasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomingasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
