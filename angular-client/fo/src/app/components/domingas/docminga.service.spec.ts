import { TestBed, inject } from '@angular/core/testing';

import { DocmingaService } from './docminga.service';

describe('DocmingaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocmingaService]
    });
  });

  it('should be created', inject([DocmingaService], (service: DocmingaService) => {
    expect(service).toBeTruthy();
  }));
});
