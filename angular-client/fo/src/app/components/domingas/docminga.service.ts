import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Dominga} from '../../interfaces/dominga';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable()
export class DocmingaService {

  constructor(private http: HttpClient) { }

  getlista(): Observable<Dominga[]> {
    return this.http.get<Dominga[]>( '/api/Dominga/lista');
  }
}
