import { Injectable } from '@angular/core';
import {Dominga} from '../../interfaces/dominga';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Irmao} from '../../interfaces/irmao';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RegistarIrmaoService {

  constructor(private http: HttpClient) { }

  registar(irmao: Irmao) {
    return this.http.put<Irmao>( '/api/irmao/', irmao , httpOptions );
  }
}



