import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistarIrmaoComponent } from './registar-irmao.component';

describe('RegistarIrmaoComponent', () => {
  let component: RegistarIrmaoComponent;
  let fixture: ComponentFixture<RegistarIrmaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistarIrmaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistarIrmaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
