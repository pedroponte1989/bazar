import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, NgForm} from '@angular/forms';
import {Irmao} from '../../interfaces/irmao';
import { v4 as uuid } from 'uuid';
import {RegistarIrmaoService} from './registar-irmao.service';

@Component({
  selector: 'app-registar-irmao',
  templateUrl: './registar-irmao.component.html',
  styleUrls: ['./registar-irmao.component.scss']
})
export class RegistarIrmaoComponent implements OnInit {

  irmao: Irmao;
  uuid: string = uuid();
  constructor( private registarIrmaoService:RegistarIrmaoService) { }
  profileForm = new FormGroup({
    idIrmaos: new FormControl(''),
    nome: new FormControl(''),
    rua: new FormControl(''),
    nporta: new FormControl(''),
    freguesia: new FormControl('')
  });

  ngOnInit() {
    this.profileForm.controls['idIrmaos'].setValue('' + this.uuid);
  }
  guardar() {

  }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.irmao = this.profileForm.value;
    console.warn(this.irmao);
    console.warn(this.profileForm.valid);
    if (this.profileForm.valid) {
      this.registarIrmaoService.registar(this.irmao).subscribe(res => alert('ok'));
    }
  }
}
