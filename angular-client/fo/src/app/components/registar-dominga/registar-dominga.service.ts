import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Produto} from "../../interfaces/produto";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Irmao} from "../../interfaces/irmao";
import {Dominga} from '../../interfaces/dominga';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};
@Injectable({
  providedIn: 'root'
})
export class RegistarDomingaService {

  constructor(private http: HttpClient) { }

  getIrmaos(): Observable<Irmao[]> {
    return this.http.get<Irmao[]>( '/api/irmao');
  }
  putDominga(produto: Dominga[]) {
    return this.http.put<Dominga[]>( '/api/Dominga/dominga/', produto , httpOptions );
  }
}
