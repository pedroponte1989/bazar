import { Pipe, PipeTransform } from '@angular/core';
import {Irmao} from '../../interfaces/irmao';

@Pipe({
  name: 'nomeIrmao'
})
export class NomeIrmaoPipe implements PipeTransform {

  transform(produtos: Irmao[], numero: string): any[] {
    if (!produtos) { return []; }
    if (!numero) { return produtos; }

    return produtos.filter( function(produtos) {

      return produtos.nome.toString().includes(numero.toString());
    });
  }

}
