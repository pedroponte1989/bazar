import { Component, OnInit } from '@angular/core';
import {RegistarDomingaService} from './registar-dominga.service';
import {Irmao} from '../../interfaces/irmao';
import {Dominga} from '../../interfaces/dominga';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Produto} from '../../interfaces/produto';
import { v4 as uuid } from 'uuid';
@Component({
  selector: 'app-registar-dominga',
  templateUrl: './registar-dominga.component.html',
  styleUrls: ['./registar-dominga.component.scss']
})
export class RegistarDomingaComponent implements OnInit {
  title: string;
  irmaos: Irmao[];
  domingas: Dominga[] = new Array();
  domingaSel: Irmao;
  numero: string;
  anoSel = new Date().getFullYear();
  calendario = [new Date().getFullYear(), new Date().getFullYear() + 1];
  constructor(private registarDominga: RegistarDomingaService) {

  }

  ngOnInit() {
    this.getIrmaos();
    for (let i = 0; i < 7; i++) {
      const irmao: Irmao =  {idirmao: 0, nome: '', rua: 'null', nporta: 'null', freguesia: 'null', visivel: 'null' };
      const aux: Dominga = { idDominga: '', irmao_idIrmao: irmao, ano: this.anoSel, bit: 0, diferenca: 0, numero: i + 1};
      this.domingas.push(aux);
    }
    console.log(this.domingas);
  }
  getIrmaos() {
    this.registarDominga.getIrmaos().subscribe(res => this.irmaos = res);
  }
  pintar(id1: number, id2: number) {
    $('#' + id1).css('background-color', 'red');
    $('#' + id2).css('background-color', 'white');
  }
  selecionar(irmao: Irmao) {
    console.log(irmao.idirmao);
    console.log('new uid: ', uuid());
    $('#btn' + irmao.idirmao).show();

    $('#A' + irmao.idirmao).css('background-color', '#007bff');
    if (this.domingaSel) {
      $('#A' + this.domingaSel.idirmao).css('background-color', 'white');
      this.domingaSel = irmao;
    }
    this.domingaSel = irmao;

    if ( this.title === '1ª Dominga') {
      const aux: Dominga = {idDominga: uuid(), irmao_idIrmao: this.domingaSel, ano: this.anoSel, bit: 0, diferenca: 0, numero: 1};

      this.domingas[0] = aux;
  }
    if ( this.title === '2ª Dominga') {
      const aux: Dominga = {idDominga:  uuid(), irmao_idIrmao: this.domingaSel, ano: this.anoSel, bit: 0, diferenca: 0, numero: 2};

      this.domingas[1] = aux;
  }
    if ( this.title === '3ª Dominga') {
      const aux: Dominga = {idDominga:  uuid(), irmao_idIrmao: this.domingaSel, ano: this.anoSel, bit: 0, diferenca: 0, numero: 3};

      this.domingas[2] = aux;
  }
    if ( this.title === '4ª Dominga') {
      const aux: Dominga = {idDominga:  uuid(), irmao_idIrmao: this.domingaSel, ano: this.anoSel, bit: 0, diferenca: 0, numero: 4};

      this.domingas[3] = aux;
  }
    if ( this.title === '5ª Dominga') {
      const aux: Dominga = {idDominga:  uuid(), irmao_idIrmao: this.domingaSel, ano: this.anoSel, bit: 0, diferenca: 0, numero: 5};

      this.domingas[4] = aux;
  }
    if ( this.title === '6ª Dominga') {
      const aux: Dominga = {idDominga:  uuid(), irmao_idIrmao: this.domingaSel, ano: this.anoSel, bit: 0, diferenca: 0, numero: 6};

      this.domingas[5] = aux;
  }
    if ( this.title === '7ª Dominga') {
      const aux: Dominga = {idDominga: uuid(), irmao_idIrmao: this.domingaSel, ano: this.anoSel, bit: 0, diferenca: 0, numero: 7};

      this.domingas[6] = aux;
  }
    // $('#modal .close').click();
    // $('input[name="tasknote"]').prop('checked', false);
}
  registar() {
    this.registarDominga.putDominga(this.domingas).subscribe();
      }
      selecionarAno(a) {
 console.log(a) ;
 this.anoSel = a;
      }
}
