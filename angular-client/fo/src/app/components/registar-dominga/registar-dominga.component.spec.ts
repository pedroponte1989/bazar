import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistarDomingaComponent } from './registar-dominga.component';

describe('RegistarDomingaComponent', () => {
  let component: RegistarDomingaComponent;
  let fixture: ComponentFixture<RegistarDomingaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistarDomingaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistarDomingaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
