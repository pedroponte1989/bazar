package pt.pedroponte.bazar.Rest.Domingas;



public class JsonDominga {

    public String idDominga;

    public JsonIrmao irmao_idIrmao;

    public  Integer ano;

    public  Integer numero;

    public  Integer diferenca;

    public  Integer bit;

    public String getidDominga() {
        return idDominga;
    }

    public void setidDominga(String idDominga) {
        this.idDominga = idDominga;
    }

    public JsonIrmao getNome() {
        return irmao_idIrmao;
    }

    public void setNome(JsonIrmao nome) {
        this.irmao_idIrmao = nome;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getDiferenca() {
        return diferenca;
    }

    public void setDiferenca(Integer diferenca) {
        this.diferenca = diferenca;
    }

    public Integer getBit() {
        return bit;
    }

    public void setBit(Integer bit) {
        this.bit = bit;
    }
}
