package pt.pedroponte.bazar.Rest.Domingas;



public class JsonIrmao {

    public String idirmao;

    public String nome;

    public  String rua;

    public  String nporta;

    public  String freguesia;

    public  String visivel;

    public String getIdIrmaos() {
        return idirmao;
    }

    public void setIdIrmaos(String idIrmaos) {
        this.idirmao = idIrmaos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNporta() {
        return nporta;
    }

    public void setNporta(String nporta) {
        this.nporta = nporta;
    }

    public String getFreguesia() {
        return freguesia;
    }

    public void setFreguesia(String freguesia) {
        this.freguesia = freguesia;
    }

    public String getVisivel() {
        return visivel;
    }

    public void setVisivel(String visivel) {
        this.visivel = visivel;
    }
}
