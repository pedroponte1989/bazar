package pt.pedroponte.bazar.Rest.Domingas;

import org.apache.log4j.Logger;
import pt.pedroponte.bazar.Rest.Bazar.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.*;

@Stateless
@Path("/Dominga")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class DomingaService implements Serializable {
    @Context
    HttpServletRequest request;

    @PersistenceContext(unitName = "mydb", type = PersistenceContextType.TRANSACTION)
    EntityManager entityManager;


    private static final Logger logger = Logger.getLogger(SaidaService.class);

    @GET
    @Path("/irmaos")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Collection<Irmao> saida() {
        return entityManager.createQuery("from Irmao", Irmao.class).getResultList();

    }
    @PUT
    @Consumes("application/json;charset=utf-8")
    @Path("/dominga")
    public String entrada(List<JsonDominga> jsonDomingas){


        for(JsonDominga x:jsonDomingas) {

            Dominga dominga = new Dominga();
            dominga.setidDominga(x.idDominga);
            dominga.setNumero(x.numero);
            dominga.setAno(x.ano);
            dominga.setBit(0);

            Irmao irmao = entityManager.find(Irmao.class, x.irmao_idIrmao.idirmao);
            dominga.setirmao_idIrmao(irmao);
            logger.info(dominga.toString());


            String query = "INSERT INTO `Dominga` (`idDominga`, `irmao_idirmao`, `numero`, `ano`, `bit`) VALUES (\'" + dominga.getidDominga() + "\',\'" + dominga.getirmao_idIrmao().getidirmao().toString() + "\', " + dominga.getNumero() + ", " + dominga.getAno() + "," + dominga.getBit() + ")";
            logger.info(query);
            entityManager.createNativeQuery(query).executeUpdate();
        }
        return  "ok";




    }

    @GET
    @Path("/lista")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Dominga> lista() {

       List<Dominga> domingas=  entityManager.createQuery("from Dominga ", Dominga.class).getResultList();
        Collections.sort(domingas, new Comparator<Dominga>() {
            @Override
            public int compare(Dominga u1, Dominga u2) {
                return ((String) ""+u1.getAno()).compareTo((String) ""+u2.getAno());
            }
        });

        Collections.sort(domingas, new Comparator<Dominga>() {
            @Override
            public int compare(Dominga u1, Dominga u2) {
                return ((String) ""+u1.getirmao_idIrmao().getidirmao()).compareTo((String) ""+u2.getirmao_idIrmao().getidirmao());
            }
        });

        for (Dominga dominga:domingas) {
            for (int i = 0; i < domingas.size() ; i++) {
                if (dominga.getirmao_idIrmao().getidirmao() == domingas.get(i).getirmao_idIrmao().getidirmao()) {
                    if (dominga.getAno() > domingas.get(i).getAno()) {
                        domingas.get(i).setBit(1);
                        dominga.setBit(0);

                    }

                }
            }
        }

        Collections.sort(domingas, new Comparator<Dominga>() {
            @Override
            public int compare(Dominga u1, Dominga u2) {
                return u1.getirmao_idIrmao().getNome().compareTo(u2.getirmao_idIrmao().getNome());
            }
        });
        Collections.sort(domingas, new Comparator<Dominga>() {
            @Override
            public int compare(Dominga u1, Dominga u2) {
                return ((String) ""+u1.getNumero()).compareTo((String) ""+u2.getNumero());
            }
        });
        Collections.sort(domingas, new Comparator<Dominga>() {
            @Override
            public int compare(Dominga u1, Dominga u2) {
                return ((String) ""+u2.getAno()).compareTo((String) ""+u1.getAno());
            }
        });

       return domingas;

    }

}
