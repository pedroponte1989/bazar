package pt.pedroponte.bazar.Rest.Domingas;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pt.pedroponte.bazar.Rest.Bazar.Produtos;
import pt.pedroponte.bazar.Rest.Bazar.Saida;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "Irmao")
@XmlRootElement
public class Irmao implements Serializable {
    @Id
    @Column(name = "idirmao")
    private String idirmao;
    @Column(name = "nome")
    private  String nome;

    @Column(name = "rua")
    private String rua;

    @Column(name = "nporta")
    private String nporta;

    @Column(name = "freguesia")
    private  String freguesia;

    @Column(name = "visivel")
    private  String visivel;

    @OneToMany(mappedBy = "irmao_idirmao")
    private List<Dominga> tipoAtos = new ArrayList<Dominga>();

    public String getidirmao() {
        return idirmao;
    }

    public void setidirmao(String idirmao) {
        this.idirmao = idirmao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNporta() {
        return nporta;
    }

    public void setNporta(String nporta) {
        this.nporta = nporta;
    }

    public String getFreguesia() {
        return freguesia;
    }

    public void setFreguesia(String freguesia) {
        this.freguesia = freguesia;
    }

    public String getVisivel() {
        return visivel;
    }

    public void setVisivel(String visivel) {
        this.visivel = visivel;
    }
}
