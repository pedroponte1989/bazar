package pt.pedroponte.bazar.Rest.Bazar;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Set;

@Entity
//@IdClass(JornalPK.class)
@Table(name = "produtos")
@XmlRootElement
public class Produtos implements Serializable {
    @Id
    @Column(name = "idprodutos")
    private int idprodutos;
    @Column(name = "nome")
    private  String nome;

    @ManyToOne(cascade = { CascadeType.PERSIST })
    @JoinColumn(name = "categorias_idcategorias")
    private Categoria categoria;

    @OneToMany(mappedBy="produtos")
    private Set<Saida> saida;



    public int getIdprodutos() {
        return idprodutos;
    }

    public void setIdprodutos(int idprodutos) {
        this.idprodutos = idprodutos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

}
