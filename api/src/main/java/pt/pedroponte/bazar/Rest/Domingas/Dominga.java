package pt.pedroponte.bazar.Rest.Domingas;

import pt.pedroponte.bazar.Rest.Bazar.Produtos;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "Dominga")
@XmlRootElement
public class Dominga implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "idDominga")
    private String idDominga;

    @ManyToOne(cascade = { CascadeType.PERSIST })
    @JoinColumn(name = "irmao_idirmao")
    private Irmao irmao_idirmao;

    @Column(name = "ano")
    private  int ano;
    @Column(name = "numero")
    private int numero;

    private int bit;

    public int getBit() {
        return bit;
    }

    public void setBit(int bit) {
        this.bit = bit;
    }

    public String getidDominga() {
        return idDominga;
    }

    public void setidDominga(String idDominga) {
        this.idDominga = idDominga;
    }

    public Irmao getirmao_idIrmao() {
        return irmao_idirmao;
    }

    public void setirmao_idIrmao(Irmao irmao_idIrmao) {
        this.irmao_idirmao = irmao_idIrmao;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Dominga{" +
                "idDominga='" + idDominga + '\'' +
                ", irmao_idirmao=" + irmao_idirmao +
                ", ano=" + ano +
                ", numero=" + numero +
                ", bit=" + bit +
                '}';
    }
}
