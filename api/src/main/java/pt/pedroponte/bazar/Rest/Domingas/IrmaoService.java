package pt.pedroponte.bazar.Rest.Domingas;

import org.apache.http.protocol.ResponseServer;
import org.apache.log4j.Logger;
import pt.pedroponte.bazar.Rest.Bazar.SaidaService;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.util.*;

@Stateless
@Path("/irmao")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class IrmaoService implements Serializable {
    @Context
    HttpServletRequest request;

    @PersistenceContext(unitName = "mydb", type = PersistenceContextType.TRANSACTION)
    EntityManager entityManager;


    private static final Logger logger = Logger.getLogger(SaidaService.class);

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Collection<Irmao> saida() {
        return entityManager.createQuery("from Irmao", Irmao.class).getResultList();

    }
    @PUT
    @Consumes("application/json;charset=utf-8")
    @Path("/")
    public void entrada(JsonIrmao jsonirmao) {

     Irmao irmao = new Irmao();

     irmao.setidirmao(jsonirmao.idirmao);
     irmao.setNome(jsonirmao.nome);
     irmao.setRua(jsonirmao.rua);
     irmao.setFreguesia(jsonirmao.freguesia);
     irmao.setNporta(jsonirmao.nporta);
     irmao.setVisivel(jsonirmao.visivel);

            logger.info(irmao.getNome());

            entityManager.merge(irmao);



    }



}
