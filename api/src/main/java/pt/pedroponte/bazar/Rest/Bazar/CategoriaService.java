package pt.pedroponte.bazar.Rest.Bazar;

import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Stateless
@Path("/categoria")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class CategoriaService {
    @Context
    HttpServletRequest request;

    @PersistenceContext(unitName = "Bazar", type = PersistenceContextType.TRANSACTION)
    EntityManager entityManager;


    private static final Logger logger = Logger.getLogger(CategoriaService.class);
    @GET
    @Path("/lista")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Collection<Categoria> lista() {
        return  entityManager.createQuery("from Categoria ", Categoria.class).getResultList();

    }
}
