package pt.pedroponte.bazar.Rest.Bazar;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "saidas")
@XmlRootElement
public class Saida implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "idsaidas")
    private int id;
    @ManyToOne(cascade = { CascadeType.PERSIST })
    @JoinColumn(name = "produtos_idprodutos")
    private Produtos produtos;
    @Column(name = "qtd")
    private int qtd;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Produtos getProdutos() {
        return produtos;
    }

    public void setProdutos(Produtos produtos) {
        this.produtos = produtos;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }
}
