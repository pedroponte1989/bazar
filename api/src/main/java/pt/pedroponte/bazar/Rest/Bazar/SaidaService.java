package pt.pedroponte.bazar.Rest.Bazar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.*;

@Stateless
@Path("/saida")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")

public class SaidaService implements Serializable {
    @Context
    HttpServletRequest request;

    @PersistenceContext(unitName = "Bazar", type = PersistenceContextType.TRANSACTION)
    EntityManager entityManager;


    private static final Logger logger = Logger.getLogger(SaidaService.class);

    @GET
    @Path("/saida")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Collection<Saida> saida() {
        return entityManager.createQuery("from Saida ", Saida.class).getResultList();


    }

    @GET
    @Path("/lista")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response lista() {
        String sql = "SELECT produtos.*,  SUM(saidas.`qtd`)  AS qtd from saidas inner join produtos on saidas.produtos_idprodutos = produtos.idprodutos  group by saidas.produtos_idprodutos";
        Query query = null;
        query = entityManager.createNativeQuery(sql);
        List list = query.getResultList();

        Gson gson = new Gson();
        Type listType = new TypeToken<List>() {
        }.getType();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size() ; i++) {
            Map<String, Object> map = null;
            map = new HashMap<String, Object>();
            String jsonInString = gson.toJson(list.get(i));
            List lst = gson.fromJson(jsonInString, listType);
            map.put("id", lst.get(0));
            map.put("descricao", lst.get(1));
            map.put("categoria", lst.get(2));
            map.put("qtd", lst.get(3));
            arrayList.add(map);
        }

        return Response.ok(arrayList, MediaType.APPLICATION_JSON).build();
    }

    @PUT
    @Consumes("application/json;charset=utf-8")
    @Path("/entrada/")
    public Response entrada(JsonProduto jsonProduto) {
        Saida saida = new Saida();
        saida.setProdutos(entityManager.find(Produtos.class,Integer.parseInt( jsonProduto.id)));
        saida.setQtd(Integer.parseInt( jsonProduto.qtd));
        entityManager.merge(saida);

        return this.lista();
    }

    @GET
    @Path("/gerarBilhetes")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response gerarBilhetes() {
        ArrayList<Integer> premios= new ArrayList<>();
        final List<Saida> saida = entityManager.createQuery("from Saida ", Saida.class).getResultList();
        for (Saida s:saida) {
            for (int i=0; i< s.getQtd(); i++){
                premios.add(s.getProdutos().getIdprodutos());
            }
        }
        Collections.sort(premios);
        return Response.ok(premios).build();
    }

}
