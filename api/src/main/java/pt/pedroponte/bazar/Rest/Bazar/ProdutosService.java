package pt.pedroponte.bazar.Rest.Bazar;

import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Stateless
@Path("/produtos")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")

public class ProdutosService implements Serializable {
    @Context
    HttpServletRequest request;

    @PersistenceContext(unitName = "Bazar", type = PersistenceContextType.TRANSACTION)
    EntityManager entityManager;


    private static final Logger logger = Logger.getLogger(ProdutosService.class);
    @GET
    @Path("/lista")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Collection<Produtos> ping() {
        return  entityManager.createQuery("from Produtos ", Produtos.class).getResultList();

    }


    @GET
    @Path("/ultimoProduto/{categoria}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Produtos ultimoProduto(@PathParam("categoria") int categoria) {
        List<Produtos> cat = entityManager.createQuery("from Produtos where categoria.id=:cat order by categoria.id asc ", Produtos.class)
                .setParameter("cat", categoria).getResultList();
        return  cat.get(cat.size()-1);

    }
    @PUT
    @Consumes("application/json;charset=utf-8")
    @Path("/entrada/{id}")
    public void entrada(@PathParam("id") String id) {
        Produtos saida = new Produtos();
        saida.setIdprodutos(10);
        saida.setNome("ola");
        saida.setCategoria(null);
        entityManager.getTransaction().begin();
        entityManager.persist(saida);
        entityManager.getTransaction().commit();

    }

    @PUT
    @Consumes("application/json;charset=utf-8")
    @Path("/Novo/")
    public void entrada(JsonProduto jsonProduto) {
        JsonProduto aux=   jsonProduto;
        Produtos produto = new Produtos();
        produto.setIdprodutos(Integer.parseInt(jsonProduto.id));
        produto.setNome(jsonProduto.descricao);
        Categoria categoria = entityManager.find(Categoria.class, Integer.parseInt(jsonProduto.categoria));
        produto.setCategoria(categoria);
    logger.info(produto.getIdprodutos());
        entityManager.merge(produto);

        Saida saida = new Saida();
        saida.setProdutos(entityManager.find(Produtos.class,Integer.parseInt( jsonProduto.id)));
        saida.setQtd(Integer.parseInt( jsonProduto.qtd));
        entityManager.merge(saida);

    }
}
